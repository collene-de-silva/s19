/*

ES
	-ECMAScript
		-standard that is used to create the implementations of the language
	-ES6
		-ECMAScript 2015
		-new features to JavaScript
		-allow us to write less and do more
*/






//Exponent Operator


	//BEFORE ES6
		/*
			syntax:
				Math.pow(base, exponent)
		*/

	const firstNum = Math.pow(8, 2)
	console.log(firstNum) //64


	//AFTER ES6
		//added exponent operator (**)
		/*
			syntax: base ** exponent
		*/

	const secondNum = 8 ** 2;
	console.log(secondNum) //64

	const thirdNum = 5 ** 3;
	console.log(thirdNum) //125




//Template Literals
	/*
		It allows us to write strings without using operator (+)
	*/

	let name = 'John'

	//BEFORE ES6
	let message = 'Hello ' + name + '! Welcome to programming.'
	console.log(message)

	//AFTER ES6
	//uses the backticks (``)
		/*
			`` - template literal
			[] - array literal
			{} - object literals
			${} - placeholder
		*/
	message = `Hello ${name}! Welcome to programming!`
	console.log(message)

	//Multi-line Using Template Literals
	const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.



	`

	console.log(anotherMessage)

	const interestRate = .1
	const principal = 1000
	console.log (`The interest on your savings is: ${principal * interestRate}`)

//Array Destructuring
	/*
	Allow us to unpack elements in arrays into distinct variables.
	-It allow us to name array elements with variables instead of using the index number.
	-ORDER IS IMPORTANT

	Syntax:
		let/const [variableName, variableName] = array

	*/

const fullName = ['Joe', 'Dela', 'Cruz']

	//BEFORE ES6
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}. It's nice to see you!`)

	//AFTER ES6
	const [firstName, middleName, lastName, anotherName] = fullName;
	console.log(firstName)
	console.log(middleName)
	console.log(lastName)
	console.log(anotherName)

	//to skip just use ,, 
/*	const [firstName,, lastName] = fullName;
	console.log(firstName)
	console.log(lastName)*/








//Object Destructuring
/*
	Allow us to unpack properties of objects into distinct variables.
	It shortens the syntax for accessing properties from objects
	-ORDER IS NOT IMPORTANT
	-CASE SENSTIVE NAME IS IMPORTANT 

	Syntax:
		let/const {propertyName, propertyName} = object;
*/

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	}

	//BEFORE ES6
	console.log(person.givenName)
	console.log(person.maidenName)
	console.log(person.familyName)

	//AFTER ES6
	const {givenName, maidenName, familyName} = person;
	console.log(givenName)

	function getFullName({givenName, maidenName, familyName}) {
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}

	getFullName(person)

//Arrow Function
/*
	-Compact alternative syntax to traditional functions
	-Useful for code snippets where creating a function will not be reused in any other portion of the code


	Syntax:
		const variableName = ( ) => {
			console.log()
		}
*/

const hello = () => {
	console.log("Hello World");
}

hello()

	//BEFORE ES6
	/*
		Syntax:
			function functionName (parameterA, parameterB) {
				console.log()
			}
	*/

/*function printFullName(firstName, middleInitial, lastName) {
	console.log(firstName + ' ' + middleInitial + ' ' + lastName)
}

printFullName('John', 'D.', 'Smith')*/

	//AFTER ES6
	/*
		Syntax:
			let/const variableName = (parameterA, parameterB) => {
				statement
			}

	*/

	const printFullName = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}

	printFullName('Mark', 'D.', 'Smith')

	//Another example
	//BEFORE ES6
	const students = ['Corazon', 'Luwalhati', 'Maria'];
	students.forEach(
				function(student) {
					console.log(`${student} is a student`)
				}

		)

	//Arrow Function

	students.forEach( (student) => {
		console.log(`${student} is a student`)
		})






//Implicit return statement
/*
	There are instances when you can omit the "return" statement
*/

	//BEFORE ES6
	/*function add(x,y) {
		return x + y
	}

	let total = add(1,2)
	console.log(total)*/

	//AFTER ES6
	const add = (x, y) => x + y;
	let total = add(1,2)
	console.log(total)


//Default function argument value

const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}

console.log(greet()) //Good morning, User! 

/*const greet = (name) => {
	return `Good morning, ${name}!`
}

console.log(greet()) // Undefined*/





//Class-based Object Blueprint
/*
	Allows creation/instantiation of objects using classes as blueprints

	The Constructor is a special method for a class for creating/initializing an object for that class


	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = 'Ford'
myCar.name = 'Ranger Raptor'
myCar.year = 2021

console.log(myCar)

const myNewCar = new Car('BMW', 'Hot Wheels', 2021)
console.log(myNewCar)