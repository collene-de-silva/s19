//getting cube using arrow function and template literal

const getCube = (number) => {
	console.log(`The cube of ${number} is ${number ** 3}`)
}

getCube(5)



//address array destructuring
const address = [258, 'Washington Ave NW', 'California', 90011]

const [houseNumber, state, country, zipCode] = address

console.log(`I live at ${houseNumber} ${state}, ${country} ${zipCode}`)


//animal object destructuring
const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: 1075,
	lengthFeet: 20,
	lengthIn: 3
}

const {name, type, weight, lengthFeet, lengthIn} = animal

console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${lengthFeet} ft ${lengthIn} in.`)


//number for each, arrow function, explicit
const numbers = [1,2,3,4,5]

numbers.forEach( (numbers) => console.log(numbers) )


//dog class

class Dog {
	constructor(name,age,breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog('Frankie',5,'Miniature Dachshund')
console.log(myDog)